package me.systemoutprintln.lugsapienza.registrationserver;

import com.google.api.services.gmail.model.Message;
import me.systemoutprintln.lugsapienza.registrationserver.gmail.EmailUtil;
import me.systemoutprintln.lugsapienza.registrationserver.gmail.GmailApi;
import me.systemoutprintln.lugsapienza.registrationserver.loggers.GmailLogger;
import me.systemoutprintln.lugsapienza.registrationserver.loggers.RequestLogger;
import me.systemoutprintln.lugsapienza.registrationserver.loggers.VerificationLogger;
import me.systemoutprintln.lugsapienza.registrationserver.model.CompleteForm;
import me.systemoutprintln.lugsapienza.registrationserver.model.Form;
import me.systemoutprintln.lugsapienza.registrationserver.util.VerificationCode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashSet;


/**
 *  Contorllers for handling all requests on:
 *  /, /confirm, /submit routes
 *
 */
@Controller
public class IndexController {

    @Value("${corso}")
    String corso;

    HashSet<String> verifiedIp = new HashSet<>();


    /**
     * Pagina di inserimento email
     * @param model
     * @param request
     * @return
     */
    @GetMapping("/")
    public String index(Model model, HttpServletRequest request){
        if(verifiedIp.contains(request.getRemoteHost())&&!request.getRemoteHost().equals("0:0:0:0:0:0:0:1"))
            return "already";
        RequestLogger.logRequest(request);
        model.addAttribute("form", new Form());
        return "presenze";
    }

    /**
     *  Pagina derivante il "POST" per email.
     *  Manda meial di conferma.
     * @param model
     * @param form
     * @param request
     * @return Pagina di verifica codice.
     */
    @PostMapping("/confirm")
    public String confirm(Model model, @ModelAttribute Form form, HttpServletRequest request){
        if(verifiedIp.contains(request.getRemoteHost())&&!request.getRemoteHost().equals("0:0:0:0:0:0:0:1"))
            return "already";
        CompleteForm completeForm = new CompleteForm();
        completeForm.setEmail(form.getEmail());
        model.addAttribute("completeform",completeForm);
        RequestLogger.logRequest(request);

//        RequestLogger.logInfo(RequestFormatter.format(request));
        String vcode = VerificationCode.getRandomNumberString();

        //aggiungo il codice
        VerificationCode.codes.put(form.getEmail(), vcode);
        try {
            MimeMessage mail = EmailUtil.createEmail(form.getEmail(), vcode, corso);
            Message m = GmailApi.sendMessage(mail);
            GmailLogger.logInfo(m.toPrettyString());

            if(m.getLabelIds().contains("SENT")){
                return "confirm";
            }else{
                return "error";
            }
        } catch (MessagingException e) {
            GmailLogger.logInfo("Error creating email for "+form.getEmail()+ " " + e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "error";

    }

    @PostMapping("/submit")
    public String submit(@ModelAttribute CompleteForm cForm, HttpServletRequest request){
        if(verifiedIp.contains(request.getRemoteHost())&&!request.getRemoteHost().equals("0:0:0:0:0:0:0:1"))
            return "already";
        RequestLogger.logRequest(request);
        VerificationLogger.logInfo(cForm.getEmail() +" "+cForm.getVcode());

        if(VerificationCode.codes.containsKey(cForm.getEmail())&&cForm.getVcode().equals(VerificationCode.codes.get(cForm.getEmail()))){
                VerificationCode.codes.remove(cForm.getEmail());
                verifiedIp.add(request.getRemoteHost());
                VerificationLogger.logInfo(cForm.getEmail() + " " + cForm.getVcode() + " SUCCESS");
                return "success";
        }else{
            VerificationLogger.logInfo(cForm.getEmail() +" "+cForm.getVcode()+" FAILURE");
            return "failure";
        }

    }
}
