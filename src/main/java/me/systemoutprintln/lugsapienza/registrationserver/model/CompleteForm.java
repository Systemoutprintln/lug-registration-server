package me.systemoutprintln.lugsapienza.registrationserver.model;

/**
 *  Model for the last form.
 */
public class CompleteForm {
    private String email;
    private String vcode;

    public CompleteForm() {
//        this.email = email;
    }

    public String getVcode() {
        return vcode;
    }

    public void setVcode(String vcode) {
        this.vcode = vcode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
