package me.systemoutprintln.lugsapienza.registrationserver.model;

/**
 *  Model for the first form.
 */
public class Form {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
