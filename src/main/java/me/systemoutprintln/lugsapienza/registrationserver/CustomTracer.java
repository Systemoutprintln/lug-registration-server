package me.systemoutprintln.lugsapienza.registrationserver;

import me.systemoutprintln.lugsapienza.registrationserver.loggers.RequestLogger;
import org.springframework.boot.actuate.trace.http.HttpTrace;
import org.springframework.boot.actuate.trace.http.InMemoryHttpTraceRepository;
import org.springframework.stereotype.Component;

/**
 *  Custom InMemoryHttpTraceRepository.
 *  It has exanded capacity and uses request logger to log everything.
 */
@Component
public class CustomTracer extends InMemoryHttpTraceRepository {

    public CustomTracer() {
        super.setCapacity(500);
    }

    @Override
    public void add(HttpTrace trace) {
        RequestLogger.logTrace(trace);
        super.add(trace);
    }
}
