package me.systemoutprintln.lugsapienza.registrationserver.gmail;

import me.systemoutprintln.lugsapienza.registrationserver.loggers.GmailLogger;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public class EmailUtil {
    private static String from = "me";
    private static String object = "Registrazione presenza %s - Verifica indirizzo email";
    private static String contenuto = "Salve, \n" +
        "abbiamo ricevuto una richiesta di registrazione presenza, per il corso di %s, con questo indirizzo email.\n" +
        "\n" +
        "Per confermare, inserisci il seguente codice, nella pagina di verifica:\n" +
        "%s\n" +
        "\n" +
        "Se non hai eseguito tu questa richiesta, cestina pure questo messaggio.\n" +
        "Grazie.\n" +
        "\n" +
        "\n" +
        "Il team del LUG Sapienza";


    public static MimeMessage createEmail(String email, String vcode, String corso) throws MessagingException {
        System.out.println(String.format(object, corso));
        System.out.println(String.format(contenuto, corso ,vcode));
        return GmailApi.createEmail(email, from,String.format(object, corso), String.format(contenuto, corso ,vcode));
    }
}
