package me.systemoutprintln.lugsapienza.registrationserver;

import me.systemoutprintln.lugsapienza.registrationserver.gmail.GmailApi;
import me.systemoutprintln.lugsapienza.registrationserver.loggers.GmailLogger;
import me.systemoutprintln.lugsapienza.registrationserver.loggers.RequestLogger;
import me.systemoutprintln.lugsapienza.registrationserver.loggers.VerificationLogger;
import me.systemoutprintln.lugsapienza.registrationserver.util.VerificationCode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main class for registration servers.
 *
 */
@SpringBootApplication
public class RegistrationserverApplication {

    /**
     * Main method to start and initialize all loggers.
     * @param args is an array with that takes a mandatory parameter and an optional parameter. In sequence <\corso> and [port]
     */
    public static void main(String[] args) {
        switch (args.length){
            case 2:
                System.setProperty("server.port", args[1]);
            case 1:
                System.setProperty("corso", args[0]);
                break;
            default:
                usage();
                return;
        }
        try {
            GmailApi.init();

        } catch (IOException | GeneralSecurityException e) {
            Logger.getLogger(RegistrationserverApplication.class.getSimpleName()).log(Level.WARNING,"Error initializing GmaiApi.");
        }
        GmailLogger.init();
        RequestLogger.init();
        VerificationLogger.init();

        SpringApplication.run(RegistrationserverApplication.class, args);

    }

    /**
     *  Prints usage when called.
     */
    protected static void usage(){
        System.out.println("Usage: java -jar <corso> [port]");
//        System.out.println("corso: ");
    }

}
