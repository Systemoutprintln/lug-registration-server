package me.systemoutprintln.lugsapienza.registrationserver.util;

import javax.servlet.http.HttpServletRequest;

/**
 *  Formatter for request
 */
public class RequestFormatter {
    public static String format(HttpServletRequest request){
        StringBuilder sb = new StringBuilder();
        System.out.println(request.getHeaderNames());
        System.out.println(request.getServerName());
        System.out.println(request.getRemoteAddr()+" "+request.getRemoteHost()+" "+request.getRemotePort());

        sb.append(request.getServerName()).append(" ")
                .append(request.getRemoteAddr()).append(" ")
                .append(request.getRemoteHost()).append(" ")
                .append(request.getRemotePort()).append(" ");
        return sb.toString();
    }
}
