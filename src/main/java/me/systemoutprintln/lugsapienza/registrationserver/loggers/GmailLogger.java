package me.systemoutprintln.lugsapienza.registrationserver.loggers;

import java.io.IOException;
import java.util.logging.*;

/**
 * Used to log gmail requests
 */
public class GmailLogger {
    private static Logger gmailLogger = Logger.getLogger("GmailLogger");

    private static FileHandler fh;

    public static void init() {
        try {

            // This block configure the logger with handler and formatter
            fh = new FileHandler("gmail.log", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        gmailLogger.addHandler(fh);
        Formatter formatter = new Formatter() {
            @Override
            public String format(LogRecord logRecord) {
                StringBuffer buf = new StringBuffer(1000);
                buf.append(new java.util.Date());
                buf.append(' ');
                buf.append(logRecord.getLevel());
                buf.append(' ');
                buf.append(formatMessage(logRecord));
                buf.append('\n');
                return buf.toString();
            }
        };
        fh.setFormatter(formatter);
        // the following statement is used to log any messages
//        requestLogger.info("My first log");

    }

    public static void logInfo(String data) {
        gmailLogger.log(Level.INFO, data);
    }


}
