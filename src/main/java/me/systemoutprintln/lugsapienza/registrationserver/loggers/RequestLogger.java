package me.systemoutprintln.lugsapienza.registrationserver.loggers;

import org.springframework.boot.actuate.trace.http.HttpTrace;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.logging.*;

/**
 *  Class used to log all requests to server
 */
public class RequestLogger {
    private static Logger requestLogger = Logger.getLogger("RequestLogger");

    private static FileHandler fh;

    public static void init() {
        try {

            // This block configure the logger with handler and formatter
                fh = new FileHandler("request.log", true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        requestLogger.addHandler(fh);
        Formatter formatter = new Formatter() {
            @Override
            public String format(LogRecord logRecord) {
                StringBuffer buf = new StringBuffer(1000);
                buf.append(new java.util.Date());
                buf.append(' ');
                buf.append(logRecord.getLevel());
                buf.append(' ');
                buf.append(formatMessage(logRecord));
                buf.append('\n');
                return buf.toString();
            }
        };
        fh.setFormatter(formatter);
    }

    public static void logInfo(String data) {

        requestLogger.log(Level.INFO, data);

    }

    public static void logError(String data){

        requestLogger.log(Level.SEVERE, data);

    }

    public static void logRequest(HttpServletRequest request){
        StringBuilder sb = new StringBuilder();
        sb.append(request.getMethod()).append(" ");
        sb.append(request.getRemoteAddr()).append(" ");
        sb.append(request.getRequestURL()).append(" ");
        sb.append(request.getRequestURI()).append(" ");
        System.out.println(sb.toString());
        requestLogger.log(Level.INFO, sb.toString());
    }

    public static void logTrace(HttpTrace request){
        StringBuilder sb = new StringBuilder();

        sb.append(request.getTimestamp()).append(" ");
        sb.append(request.getRequest().getMethod()).append("\nHeaders: ");
        sb.append(request.getRequest().getHeaders());
        sb.append("HeaderResponse ").append(request.getResponse().getHeaders()).append("\n").append(" Remote:");
        sb.append(request.getRequest().getRemoteAddress()).append(" ");
        requestLogger.log(Level.INFO, sb.toString());
        System.out.println(sb.toString());

    }
}
