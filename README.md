<h1>LUG Registration Server</h1>
<h3>Usage</h3>
Run this program using<br>

````md
   $ java -jar <corso> [port]
````
corso: parameter used for the name of course.<br>
port: optional parameter for custom port.